#ifndef __MOTIONEVENTWRAPPER_H_
#define __MOTIONEVENTWRAPPER_H_

#include <android/input.h>

class MotionEventWrapper
{
public:
    MotionEventWrapper(AInputEvent *inputEvent)
        : m_motionEvent(inputEvent)
    {
    }

    inline int32_t getDeviceId() const { return AInputEvent_getDeviceId(m_motionEvent); }

    inline int32_t getAction() const { return AMotionEvent_getAction(m_motionEvent); }

    inline int32_t getActionMasked() const { return getAction() & AMOTION_EVENT_ACTION_MASK; }

    inline int32_t getActionIndex() const
    {
        return (getAction() & AMOTION_EVENT_ACTION_POINTER_INDEX_MASK)
            >> AMOTION_EVENT_ACTION_POINTER_INDEX_SHIFT;
    }

    inline int32_t getToolType(size_t pointerIndex) const
    {
        return AMotionEvent_getToolType(m_motionEvent, pointerIndex);
    }

    inline size_t getHistorySize() const { return AMotionEvent_getHistorySize(m_motionEvent); }

    inline int64_t getHistoricalEventTime(size_t historicalIndex) const
    {
        return AMotionEvent_getHistoricalEventTime(m_motionEvent, historicalIndex);
    }

    inline int64_t getHistoricalEventTimeMs(size_t historicalIndex) const
    {
        return getHistoricalEventTime(historicalIndex) / NS_PER_MS;
    }

    inline float getHistoricalAxisValue(int32_t axis, size_t pointerIndex,
                                        size_t historicalIndex) const
    {
        return AMotionEvent_getHistoricalAxisValue(m_motionEvent, axis, pointerIndex,
                                                   historicalIndex);
    }

    inline float getHistoricalX(size_t historicalIndex) const
    {
        return getHistoricalX(0, historicalIndex);
    }

    inline float getHistoricalX(size_t pointerIndex, size_t historicalIndex) const
    {
        return AMotionEvent_getHistoricalX(m_motionEvent, pointerIndex, historicalIndex);
    }

    inline float getHistoricalY(size_t historicalIndex) const
    {
        return getHistoricalY(0, historicalIndex);
    }

    inline float getHistoricalY(size_t pointerIndex, size_t historicalIndex) const
    {
        return AMotionEvent_getHistoricalY(m_motionEvent, pointerIndex, historicalIndex);
    }

    inline float getHistoricalPressure(size_t historicalIndex) const
    {
        return getHistoricalPressure(0, historicalIndex);
    }

    inline float getHistoricalPressure(size_t pointerIndex, size_t historicalIndex) const
    {
        return AMotionEvent_getHistoricalPressure(m_motionEvent, pointerIndex, historicalIndex);
    }

    float getHistoricalTouchMajor(size_t pointerIndex, size_t historicalIndex)
    {
        return AMotionEvent_getHistoricalTouchMajor(m_motionEvent, pointerIndex, historicalIndex);
    }

    float getHistoricalTouchMinor(size_t pointerIndex, size_t historicalIndex)
    {
        return AMotionEvent_getHistoricalTouchMinor(m_motionEvent, pointerIndex, historicalIndex);
    }

    float getHistoricalOrientation(size_t pointerIndex, size_t historicalIndex)
    {
        return AMotionEvent_getHistoricalOrientation(m_motionEvent, pointerIndex, historicalIndex);
    }

    inline int64_t getEventTime() const { return AMotionEvent_getEventTime(m_motionEvent); }

    inline int64_t getEventTimeMs() const { return getEventTime() / NS_PER_MS; }

    inline float getAxisValue(int32_t axis) const
    {
        return AMotionEvent_getAxisValue(m_motionEvent, axis, 0);
    }

    inline float getX(int pointerIndex) const
    {
        return AMotionEvent_getX(m_motionEvent, pointerIndex);
    }

    inline float getX() const { return getX(0); }

    inline float getY(int pointerIndex) const
    {
        return AMotionEvent_getY(m_motionEvent, pointerIndex);
    }

    inline float getY() const { return getY(0); }

    inline float getPressure(int pointerIndex) const
    {
        return AMotionEvent_getPressure(m_motionEvent, pointerIndex);
    }

    inline float getPressure() const { return getPressure(0); }

    inline int32_t getButtonState() const { return AMotionEvent_getButtonState(m_motionEvent); }

    inline int32_t getMetaState() const { return AMotionEvent_getMetaState(m_motionEvent); }

    size_t getPointerCount() const { return AMotionEvent_getPointerCount(m_motionEvent); }

    int32_t getPointerId(size_t pointerIndex) const
    {
        return AMotionEvent_getPointerId(m_motionEvent, pointerIndex);
    }

    float getOrientation(size_t pointerIndex)
    {
        return AMotionEvent_getOrientation(m_motionEvent, pointerIndex);
    }

    float getTouchMajor(size_t pointerIndex)
    {
        return AMotionEvent_getTouchMajor(m_motionEvent, pointerIndex);
    }

    float getTouchMinor(size_t pointerIndex)
    {
        return AMotionEvent_getTouchMinor(m_motionEvent, pointerIndex);
    }

private:
    AInputEvent *m_motionEvent;

    const static long NS_PER_MS = 1000000;
};

#endif // __MOTIONEVENTWRAPPER_H_
